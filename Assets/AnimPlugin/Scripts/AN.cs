﻿/**
 * This file is a part of Unity AnimPlugin
 *
 * @author      Szymon 'Zachi' Zachara (szymon.zachara@gmail.com)
 */

using UnityEngine;
using System.Collections;
using System;

namespace AnimPlugin
{
	#region Helpers: Easing function delegate, RotationModifier
	public delegate float EasingFunction (
        float currTime,
        float beginningValue,
        float desiredValue,
        float duration);

	public struct RotationMod
	{
		public readonly bool X;
		public readonly bool Y;
		public readonly bool Z;

		public RotationMod (bool XPlus, bool YPlus, bool ZPlus)
		{
			this.X = XPlus;
			this.Y = YPlus;
			this.Z = ZPlus;
		}

		public static RotationMod Create (bool XPlus, bool YPlus, bool ZPlus)
		{
			return new RotationMod (XPlus, YPlus, ZPlus);
		}
	}
	#endregion

	public static class AN
	{
		#region static Lerp function replacement

		public static float Lerp (float minimum, float maximum, float time)
		{
			return E.Linear (time, minimum, maximum - minimum, 1f);
		}

		public static float Lerp (float minimum, float maximum, float time, EasingFunction f)
		{
			return f (time, minimum, maximum - minimum, 1f);
		}

		public static Vector2 Lerp (Vector2 minimum, Vector2 maximum, float time)
		{
			return new Vector2 (
				Lerp (minimum.x, maximum.x, time),
				Lerp (minimum.y, maximum.y, time)
			);
		}

		public static Vector2 Lerp (Vector2 minimum, Vector2 maximum, float time, EasingFunction f)
		{
			return new Vector2 (
				Lerp (minimum.x, maximum.x, time, f),
				Lerp (minimum.y, maximum.y, time, f)
			);
		}

		public static Vector3 Lerp (Vector3 minimum, Vector3 maximum, float time)
		{
			return new Vector3 (
				Lerp (minimum.x, maximum.x, time),
				Lerp (minimum.y, maximum.y, time),
				Lerp (minimum.z, maximum.z, time)
			);
		}

		public static Vector3 Lerp (Vector3 minimum, Vector3 maximum, float time, EasingFunction f)
		{
			return new Vector3 (
				Lerp (minimum.x, maximum.x, time, f),
				Lerp (minimum.y, maximum.y, time, f),
				Lerp (minimum.z, maximum.z, time, f)
			);
		}

		public static Vector4 Lerp (Vector4 minimum, Vector4 maximum, float time)
		{
			return new Vector4 (
				Lerp (minimum.x, maximum.x, time),
				Lerp (minimum.y, maximum.y, time),
				Lerp (minimum.z, maximum.z, time),
				Lerp (minimum.w, maximum.w, time)
			);
		}

		public static Vector4 Lerp (Vector4 minimum, Vector4 maximum, float time, EasingFunction f)
		{
			return new Vector4 (
				Lerp (minimum.x, maximum.x, time, f),
				Lerp (minimum.y, maximum.y, time, f),
				Lerp (minimum.z, maximum.z, time, f),
				Lerp (minimum.w, maximum.w, time, f)
			);
		}

		public static Color Lerp (Color minimum, Color maximum, float time)
		{
			return new Color (
				Lerp (minimum.r, maximum.r, time),
				Lerp (minimum.g, maximum.g, time),
				Lerp (minimum.b, maximum.b, time),
				Lerp (minimum.a, maximum.a, time)
			);
		}

		public static Color Lerp (Color minimum, Color maximum, float time, EasingFunction f)
		{
			return new Color (
				Lerp (minimum.r, maximum.r, time, f),
				Lerp (minimum.g, maximum.g, time, f),
				Lerp (minimum.b, maximum.b, time, f),
				Lerp (minimum.a, maximum.a, time, f)
			);
		}

		#endregion

		#region Performing animation on basic datatypes

		public static IEnumerator PerformFunction<V> (Func<V, V> function, V initialVal, V desiredVal) where V:IEquatable<V>
		{
			V currVal = initialVal;
			do {
				currVal = function (currVal);
				yield return null;
			} while(!currVal.Equals (desiredVal));
		}

		public static IEnumerator PerformFunction<V> (Func<V, V> function, V val, Func<V, bool> doContinue) where V:class
		{
			while (doContinue (val)) {
				val = function (val);
				yield return null;
			}
		}

		public static IEnumerator PerformFunction (Action<float> action, float initialValue, float desiderValue, float animDuration)
		{
			return PerformFunction (action, initialValue, desiderValue, animDuration, E.Linear);
		}

		public static IEnumerator PerformFunction (Action<float> action, float initialValue, float desiredValue, float animDuration, EasingFunction f)
		{
			float dF = desiredValue - initialValue;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				action (
					f (
						sumTime, 
						initialValue, 
						dF, 
						animDuration
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			action (desiredValue);
		}

		public static IEnumerator PerformFunction (Action<Vector2> action, Vector2 initialValue, Vector2 desiredValue, float animDuration)
		{
			return PerformFunction (action, initialValue, desiredValue, animDuration, E.Linear);
		}

		public static IEnumerator PerformFunction (Action<Vector2> action, Vector2 initialValue, Vector2 desiredValue, float animDuration, EasingFunction f)
		{
			float dX = desiredValue.x - initialValue.x;
			float dY = desiredValue.y - initialValue.y;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				action (
					new Vector3 (
						f (sumTime, initialValue.x, dX, animDuration),
						f (sumTime, initialValue.y, dY, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			action (desiredValue);   
		}

		public static IEnumerator PerformFunction (Action<Vector3> action, Vector3 initialValue, Vector3 desiredValue, float animDuration)
		{
			return PerformFunction (action, initialValue, desiredValue, animDuration, E.Linear);
		}

		public static IEnumerator PerformFunction (Action<Vector3> action, Vector3 initialValue, Vector3 desiredValue, float animDuration, EasingFunction f)
		{
			float dX = desiredValue.x - initialValue.x;
			float dY = desiredValue.y - initialValue.y;
			float dZ = desiredValue.z - initialValue.z;   
			float sumTime = 0f;
			while (sumTime < animDuration) {
				action (
					new Vector3 (
						f (sumTime, initialValue.x, dX, animDuration),
						f (sumTime, initialValue.y, dY, animDuration),
						f (sumTime, initialValue.y, dZ, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			action (desiredValue);   
		}

		#endregion

		#region Animations on objects

		public static IEnumerator ChangeColor (Action<Color> c, Color initialColor, Color desiredColor, float animDuration)
		{
			return ChangeColor (c, initialColor, desiredColor, animDuration, E.Linear);
		}

		public static IEnumerator ChangeColor (Action<Color> c, Color initialColor, Color desiredColor, float animDuration, EasingFunction f)
		{
			float iR = initialColor.r;
			float iG = initialColor.g;
			float iB = initialColor.b;
			float iA = initialColor.a;
			float dR = desiredColor.r - iR;
			float dG = desiredColor.g - iG;
			float dB = desiredColor.b - iB;
			float dA = desiredColor.a - iA;

			float sumTime = 0f;
			while (sumTime < animDuration) {
				c (
					new Color (
						f (sumTime, iR, dR, animDuration),
						f (sumTime, iG, dG, animDuration),
						f (sumTime, iB, dB, animDuration),
						f (sumTime, iA, dA, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//need to be sure
			c (desiredColor);
		}

		public static IEnumerator MoveTowards (Transform t, Vector3 desiredPos, float animDuration)
		{
			return MoveTowards (t, desiredPos, animDuration, E.Linear);
		}

		public static IEnumerator MoveTowards (Transform t, Vector3 desiredPos, float animDuration, EasingFunction f)
		{
			Vector3 basePosition = t.position;
			float dX = desiredPos.x - basePosition.x;
			float dY = desiredPos.y - basePosition.y;
			float dZ = desiredPos.z - basePosition.z;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				t.position = new Vector3 (
					f (sumTime, basePosition.x, dX, animDuration),
					f (sumTime, basePosition.y, dY, animDuration),
					f (sumTime, basePosition.z, dZ, animDuration)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//I need to be sure
			t.position = desiredPos;
		}

		public static IEnumerator RotateTowards (Transform t, Quaternion desiredRotation, float animDuration)
		{
			return RotateTowards (t, desiredRotation, animDuration, E.Linear);
		}

		public static IEnumerator RotateTowards (Transform t, Quaternion desiredRotation, float animDuration, EasingFunction f)
		{
			return RotateTowardsEuler (t, desiredRotation.eulerAngles, animDuration, f);
		}

		public static IEnumerator RotateTowardsEuler (Transform t, Vector3 desiredEuler, float animationDuration)
		{
			return RotateTowardsEuler (t, desiredEuler, animationDuration, E.Linear);
		}

		public static IEnumerator RotateTowardsEuler (Transform t, Vector3 desiredEuler, float animationDuration, RotationMod rmod)
		{
			return RotateTowardsEuler (t, desiredEuler, animationDuration, E.Linear, rmod);
		}

		public static IEnumerator RotateTowardsEuler (Transform t, Vector3 desiredRotation, float animDuration, EasingFunction f)
		{
			Vector3 baseVec = t.rotation.eulerAngles;
			Vector3 desVec = desiredRotation;
			float dX = (desVec.x - baseVec.x) % 360;
			float dY = (desVec.y - baseVec.y) % 360;
			float dZ = (desVec.z - baseVec.z) % 360;
			float sumTime = 0;
			while (sumTime < animDuration) {
				t.rotation = Quaternion.Euler (
					new Vector3 (
						f (sumTime, baseVec.x, dX, animDuration),
						f (sumTime, baseVec.y, dY, animDuration),
						f (sumTime, baseVec.z, dZ, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			t.rotation = Quaternion.Euler (desiredRotation);
		}

		public static IEnumerator RotateTowardsEuler (Transform t, Vector3 desiredRotation, float animDuration, EasingFunction f, RotationMod rmod)
		{
			Vector3 baseVec = t.rotation.eulerAngles;
			Vector3 desVec = desiredRotation;
			float dX = (desVec.x - baseVec.x) % 360;
			float dY = (desVec.y - baseVec.y) % 360;    
			float dZ = (desVec.z - baseVec.z) % 360;
			if (rmod.X) {
				while (baseVec.x > dX) {
					dX += 360;
				}
			} else {
				while (baseVec.x < dX) {
					dX -= 360;
				}
			}
			if (rmod.Y) {
				while (baseVec.y > dY) {
					dY += 360;
				}
			} else {
				while (baseVec.y < dY) {
					dY -= 360;
				}
			}
			if (rmod.Z) {
				while (baseVec.z > dZ) {
					dZ += 360;
				}
			} else {
				while (baseVec.z < dZ) {
					dZ -= 360;
				}
			}
			float sumTime = 0;
			while (sumTime < animDuration) {
				t.rotation = Quaternion.Euler (
					new Vector3 (
						f (sumTime, baseVec.x, dX, animDuration),
						f (sumTime, baseVec.y, dY, animDuration),
						f (sumTime, baseVec.z, dZ, animDuration)
					)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			t.rotation = Quaternion.Euler (desiredRotation);   
		}

		public static IEnumerator ScaleTo (Transform t, Vector3 desiredSize, float animDuration)
		{
			return ScaleTo (t, desiredSize, animDuration, E.Linear);
		}

		public static IEnumerator ScaleTo (Transform t, Vector3 desiredSize, float animDuration, EasingFunction f)
		{
			Vector3 baseSize = t.localScale;
			float dX = desiredSize.x - baseSize.x;
			float dY = desiredSize.y - baseSize.y;
			float dZ = desiredSize.z - baseSize.z;
			float sumTime = 0f;
			while (sumTime < animDuration) {
				t.localScale = new Vector3 (
					f (sumTime, baseSize.x, dX, animDuration),
					f (sumTime, baseSize.y, dY, animDuration),
					f (sumTime, baseSize.z, dZ, animDuration)
				);
				sumTime += Time.deltaTime;
				yield return null;
			}
			//I need to be sure
			t.localScale = desiredSize;
		}

		#endregion

		#region Special Animation Events

		public static IEnumerator Stoppable (Func<bool> condition, IEnumerator coroutine)
		{
			bool continued = true;
			while (!condition () && continued) {
				if (!coroutine.MoveNext ()) {
					continued = false;
				}
                
				yield return null;
			}
		}

		public static IEnumerator Pauseable (Func<bool> condition, IEnumerator coroutine)
		{
			bool continued = true;
			while (continued) {
				if (!condition ()) {
					if (!coroutine.MoveNext ()) {
						continued = false;
					}
				} 
				yield return null;
			}
		}

		/// <summary>
		/// Pauses animation queue for specified seconds.
		/// </summary>
		/// <param name="seconds">Seconds to wait</param>
		public static IEnumerator Wait (float seconds)
		{
			float sumTime = 0;
			while (sumTime < seconds) {
				sumTime += Time.deltaTime;
				yield return null;
			}
		}

		/// <summary>
		/// Executes the void() method as IEnumerator
		/// (so it can be inserted into Stack(params[anim]
		/// </summary>
		/// <example>
		/// <code>
		/// this.StartCoroutine(
		///     A.Stack(
		///         A.MoveTowards(this.transform, Vector3.zero, 5f),
		///         A.Wait(3f),
		///         A.Feedback(() => 
		///             {
		///                 GameObject.Destroy(this.gameobject);
		///             }
		///         )
		///     )
		/// );
		/// </code>
		/// </example>
		/// <param name="action">Action.</param>
		public static IEnumerator Feedback (Action action)
		{
			action ();
			yield return null;
		}

		/// <summary>
		/// Executes the void() method as IEnumerator
		/// (so it can be inserted into Stack(params[anim]
		/// </summary>
		/// <example>
		/// <code>
		/// this.StartCoroutine(
		///     A.Stack(
		///         A.MoveTowards(this.transform, Vector3.zero, 5f),
		///         A.Wait(3f),
		///         A.Void(() => 
		///             {
		///                 GameObject.Destroy(this.gameobject);
		///             }
		///         )
		///     )
		/// );
		/// </code>
		/// </example>
		/// <param name="action">Action.</param>
		public static IEnumerator Void (Action action)
		{
			action ();
			yield return null;
		}

		public static IEnumerator Void<V> (Action<V> action, V val)
		{
			action (val);
			yield return null;
		}

		public static IEnumerator Void<V0,V1> (Action<V0, V1> action, V0 val0, V1 val1)
		{
			action (val0, val1);
			yield return null;
		}

		#endregion

		#region MultiAnimations joining few animations together

		/// <summary>
		/// Chain the specified anims and perform them one-by-one.
		/// Next anim starts frame after previous finishes.
		/// Lasts as long as all the animations together.
		/// </summary>
		/// <param name="anims">IEnumerators to perform</param>
		public static IEnumerator Chain (params IEnumerator[] anims)
		{
			for (int i = 0; i < anims.Length; i++) {
				while (anims [i].MoveNext ()) {
					yield return anims [i].Current;
				}
			}
		}

		/// <summary>
		/// Executes all the anims at once.
		/// All anim starts immidietly.
		/// Lasts as long as the longest of IEnumerators it's performing.
		/// </summary>
		/// <param name="anims">IEnumerators to perform</param>
		public static IEnumerator Parallel (params IEnumerator[] anims)
		{
			bool any;
			do {
				any = false;
				for (int i = 0; i < anims.Length; i++) {
					if (anims [i].MoveNext ()) {
						any = true;
					}
				}
				yield return null;
			} while(any);
		}

		#endregion
	}
}  