﻿using UnityEngine;
using System.Collections;
using AnimPlugin;

/// <summary>
/// Shows parallel, chain and pauseable in action
/// </summary>
public class BouncingBallBehaviour : MonoBehaviour
{

    #region Fields

    public float Duration = 3f;
    private bool _Continuous = true;
    private EasingFunction _ChoosenFunction;
    private Vector3 _StartPosition;
    private Vector3 _EndPosition;

    #endregion

    #region MonoBehaviour methods

    void Awake()
    {
        this._StartPosition = this.transform.position;
        this._EndPosition = new Vector3(
            -1f * this.transform.position.x, 
            this.transform.position.y, 
            this.transform.position.z
        );
        this._ChoosenFunction = E.BounceIn;
    }

    void Start()
    {
        StartAnimation();
    }

    #endregion

    #region Public Methods

    public void StopAnimation()
    {
        this.StopAllCoroutines();
    }

    public void StartAnimation()
    {
        this.StartCoroutine(
            AN.Pauseable(this.IsPaused,
                AN.Chain(
                    AN.Parallel(
                        AN.MoveTowards(this.transform, this._EndPosition, this.Duration, this._ChoosenFunction),
                        AN.ChangeColor(SetC, Color.red, Color.yellow, this.Duration, this._ChoosenFunction)
                    ),
                    AN.Wait(.3f),
                    AN.Parallel(
                        AN.MoveTowards(this.transform, this._StartPosition, this.Duration, this._ChoosenFunction),
                        AN.ChangeColor(SetC, Color.yellow, Color.blue, this.Duration, this._ChoosenFunction)
                    ),
                    AN.ChangeColor(SetC, Color.blue, Color.red, .9f),
                    AN.Feedback(this.BounceFinished)
                )
            )
        );
    }

    public bool IsPaused()
    {
        return Input.GetMouseButton(0);
    }

    public void ResetState()
    {
        this.transform.position = this._StartPosition;
        this._ChoosenFunction = E.Linear;
    }

    void SetC(Color c)
    {
        this.GetComponent<Renderer>().material.color = c;
    }

    public void BounceFinished()
    {
        if (this._Continuous)
        {
            this.StartAnimation();
        }
    }

    #endregion
}
