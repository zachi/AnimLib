﻿using UnityEngine;
using System.Collections;
using AnimPlugin;

/// <summary>
/// Shows easy Lerp replacement method
/// (for projects that you've already used lerp and now want to upgrade them to AnimPlugin)
/// </summary>
public class LerpReplacement : MonoBehaviour
{
    #region Fields

    public GameObject standardLerp;
    public GameObject replacedLerp;
    public GameObject animLerpEasing;

    #endregion

    #region Mono Methods

    void Start()
    {
        this.StartCoroutine("Anims");
    }

    #endregion

    #region Coroutine

    IEnumerator Anims()
    {
        float minimum = -5f;
        float maximum = 5f;
        while (true)
        {
            // Probably in your project you have something like this:
            // (
            //      It's a slightly modified lerp moving example from Unity scripting manual :
            //      http://docs.unity3d.com/ScriptReference/Mathf.Lerp.html 
            // )
            standardLerp.transform.position = 
            new Vector3(
                Mathf.Lerp(minimum, maximum, .5f * (Mathf.Sin(Time.time) + 1)), 
                standardLerp.transform.position.y, 
                standardLerp.transform.position.z
            );

            //exact same effect with A.Lerp from AnimPlugin (replaced Mathf.Lerp to A.Lerp)
            replacedLerp.transform.position =
            new Vector3(
                AN.Lerp(minimum, maximum, .5f * (Mathf.Sin(Time.time) + 1)),
                replacedLerp.transform.position.y,
                replacedLerp.transform.position.z
            );

            //and with the code above with Easing function added as fourth paramether to Lerp function
            animLerpEasing.transform.position = 
            new Vector3(
                AN.Lerp(minimum, maximum, .5f * (Mathf.Sin(Time.time) + 1), E.SineIn),
                animLerpEasing.transform.position.y,
                animLerpEasing.transform.position.z
            );

            //There are also Lerp functions for Vectors and Color, so give them a try
            yield return false;
        }
    }

    #endregion
}
