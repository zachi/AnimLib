﻿using UnityEngine;
using System.Collections;
using AnimPlugin;

public class RotatingBoxBehaviour : MonoBehaviour
{

    public bool reverse = true;

    void Start()
    {
        Anim();
    }

    void Anim()
    {
        this.StartCoroutine(
            AN.Chain(
                AN.RotateTowardsEuler(this.transform, Vector3.zero, 4f, E.BounceInOut, RotationMod.Create(!reverse, !reverse, !reverse)),
                AN.RotateTowardsEuler(this.transform, new Vector3(30f, 70f, 90f), 3f, E.BounceInOut, RotationMod.Create(reverse, reverse, reverse)),
                AN.RotateTowardsEuler(this.transform, new Vector3(270f, 20f, 200f), 4f, E.BounceInOut, RotationMod.Create(!reverse, reverse, !reverse)),
                AN.RotateTowardsEuler(this.transform, new Vector3(0f, 0f, 90f), 3f, E.BounceInOut, RotationMod.Create(reverse, !reverse, reverse)),
                AN.Feedback(Anim)
            )
        );
    }
}
