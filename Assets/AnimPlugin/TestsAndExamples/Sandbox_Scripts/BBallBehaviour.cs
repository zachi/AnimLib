﻿using UnityEngine;
using System.Collections;
using AnimPlugin;
using System.Collections.Generic;

public class BBallBehaviour : MonoBehaviour
{

    #region Fields

    public GameObject SpawnPrefab;

    private Function _func;
    private Modifier _mod;
    private EasingFunction _easing = E.Linear;
    private GameObject LastSpawned;


    #endregion

    #region Properties

    public Function EasingFunction
    {
        get { return this._func; }
        set
        {
            this._func = value; 
            this.RecalculateEasing();
        }
    }

    public Modifier EasingModifier
    {
        get { return this._mod; }
        set
        {
            this._mod = value; 
            this.RecalculateEasing();
        }
    }

    #endregion

    private void RecalculateEasing()
    {
        this._easing = EnumHelper.EasingFunctionFromChoice(
            this._func,
            this._mod
        );
    }

    void Start()
    {
        this.JumpOrSomething();
        this.LastSpawned = this.gameObject;
        this.InvokeRepeating("LeaveTrace", 0f, .05f);
    }


    void LeaveTrace()
    {
        if (DoTrace)
        {
            GameObject trace = (GameObject)GameObject.Instantiate(
                                   this.SpawnPrefab,
                                   this.transform.position,
                                   Quaternion.identity
                               );
            if (this.LastSpawned != null)
            {
                trace.GetComponent<TraceBehaviour>().Previous = this.LastSpawned.transform.position;
            }
            else
            {
                trace.GetComponent<TraceBehaviour>().Previous = trace.transform.position;
            }
            this.LastSpawned = trace;
        }
    }

    bool DoTrace = false;

    private void JumpOrSomething()
    {
        this.DoTrace = false;
        this.LastSpawned = null;
        this.transform.position = new Vector3(this.transform.position.x, -2, this.transform.position.z);
        this.DoTrace = true;
        this.StartCoroutine(
            AN.Chain(
                AN.Feedback(LeaveTrace),
                AN.MoveTowards(this.transform, new Vector3(this.transform.position.x, 2f, this.transform.position.z), 2f, this._easing),
                AN.Feedback(LeaveTrace),
                AN.Feedback(JumpOrSomething)
            )
        );
    }
}
