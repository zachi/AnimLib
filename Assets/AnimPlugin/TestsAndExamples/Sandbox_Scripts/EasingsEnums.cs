﻿using AnimPlugin;

public enum Function
{
    Linear = 0,
    Expo = 1,
    Circural = 2,
    Quad = 3,
    Sine = 4,
    Cubic = 5,
    Quartic = 6,
    Quintic = 7,
    Elastic = 8,
    Bounce = 9,
    Back = 10
}

public enum Modifier
{
    In = 0,
    Out = 1,
    InOut = 2,
    OutIn = 3
}

public static class EnumHelper
{
    public static EasingFunction EasingFunctionFromChoice(Function f, Modifier m)
    {
        switch (f)
        {
            case Function.Linear:
                return E.Linear;
            case Function.Expo:
                switch (m)
                {
                    case Modifier.In:
                        return E.ExpoIn;
                    case Modifier.InOut:
                        return E.ExpoInOut;
                    case Modifier.Out:
                        return E.ExpoOut;
                    case Modifier.OutIn:
                        return E.ExpoOutIn;
                }
                break;
            case Function.Circural:
                switch (m)
                {
                    case Modifier.In:
                        return E.CircIn;
                    case Modifier.InOut:
                        return E.CircInOut;
                    case Modifier.Out:
                        return E.CircOut;
                    case Modifier.OutIn:
                        return E.CircOutIn;
                }
                break;
            case Function.Quad:
                switch (m)
                {
                    case Modifier.In:
                        return E.QuadIn;
                    case Modifier.InOut:
                        return E.QuadInOut;
                    case Modifier.Out:
                        return E.QuadOut;
                    case Modifier.OutIn:
                        return E.QuadOutIn;
                }
                break;
            case Function.Sine:
                switch (m)
                {
                    case Modifier.In:
                        return E.SineIn;
                    case Modifier.InOut:
                        return E.SineInOut;
                    case Modifier.Out:
                        return E.SineOut;
                    case Modifier.OutIn:
                        return E.SineOutIn;
                }
                break;
            case Function.Cubic:
                switch (m)
                {
                    case Modifier.In:
                        return E.CubicIn;
                    case Modifier.InOut:
                        return E.CubicInOut;
                    case Modifier.Out:
                        return E.CubicOut;
                    case Modifier.OutIn:
                        return E.CubicOutIn;
                }
                break;
            case Function.Quartic:
                switch (m)
                {
                    case Modifier.In:
                        return E.QuartIn;
                    case Modifier.InOut:
                        return E.QuartInOut;
                    case Modifier.Out:
                        return E.QuartOut;
                    case Modifier.OutIn:
                        return E.QuartOutIn;
                }
                break;
            case Function.Quintic:
                switch (m)
                {
                    case Modifier.In:
                        return E.QuintIn;
                    case Modifier.InOut:
                        return E.QuintInOut;
                    case Modifier.Out:
                        return E.QuintOut;
                    case Modifier.OutIn:
                        return E.QuintOutIn;
                }
                break;
            case Function.Elastic:
                switch (m)
                {
                    case Modifier.In:
                        return E.ElasticIn;
                    case Modifier.InOut:
                        return E.ElasticInOut;
                    case Modifier.Out:
                        return E.ElasticOut;
                    case Modifier.OutIn:
                        return E.ElasticOutIn;
                }
                break;
            case Function.Bounce:
                switch (m)
                {
                    case Modifier.In:
                        return E.BounceIn;
                    case Modifier.InOut:
                        return E.BounceInOut;
                    case Modifier.Out:
                        return E.BounceOut;
                    case Modifier.OutIn:
                        return E.BounceOutIn;
                }
                break;
            case Function.Back:
                switch (m)
                {
                    case Modifier.In:
                        return E.BackIn;
                    case Modifier.InOut:
                        return E.BackInOut;
                    case Modifier.Out:
                        return E.BackOut;
                    case Modifier.OutIn:
                        return E.BackOutIn;
                }
                break;
            default:
                return E.Linear;

        }
        //fallback:
        return E.Linear;
    }
}