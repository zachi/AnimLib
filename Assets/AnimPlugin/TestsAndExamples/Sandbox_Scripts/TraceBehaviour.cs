﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class TraceBehaviour : MonoBehaviour
{

    private Vector3 _Previous;

    public Vector3 Previous
    {
        set
        {
            this._Previous = value;
            this.GetComponent<LineRenderer>().SetPosition(0, this._Previous);
        }
    }
    // Use this for initialization
    void Start()
    {
        this.GetComponent<LineRenderer>().SetPosition(1, this.transform.position);
        this.Invoke("DestroyMe", 8f);
    }
	
    // Update is called once per frame
    void Update()
    {
        float delta = 2f * Time.deltaTime;
        this.Previous = new Vector3(
            this._Previous.x + delta,
            this._Previous.y,
            this._Previous.z
        );

        this.gameObject.transform.position = new Vector3(
            this.transform.position.x + delta,
            this.transform.position.y,
            this.transform.position.z
        );


        this.GetComponent<LineRenderer>().SetPosition(1, this.transform.position);
    }

    void DestroyMe()
    {
        GameObject.Destroy(this.gameObject);
    }
}
