﻿using UnityEngine;
using System.Collections;
using AnimPlugin;

public class Trail : MonoBehaviour
{
    void Update()
    {
        this.transform.Translate(Vector3.right * Time.deltaTime);
    }

    void Start()
    {
        this.StartCoroutine(
            AN.Chain(
                AN.Wait(10f),
                AN.Feedback(() =>
                {
                    GameObject.Destroy(this.gameObject);
                })
            )
        );
    }
}
