﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIHandler : MonoBehaviour
{
    #region Fields

    public GameObject FunctionPanel;
    public GameObject ModPanel;
    public GameObject BottomPanel;
    public BBallBehaviour Ball;
    public Text _FuncBtnText;
    public Text _ModBtnText;

    #endregion

    void Start()
    {
        this.OnChooseFunction(Function.Linear);
        this.OnChooseModifier(Modifier.In);
    }

    public void ExpandChooseFunction()
    {
        this.BottomPanel.SetActive(false);
        this.FunctionPanel.SetActive(true);
    }

    public void ExpandChooseModifier()
    {
        this.BottomPanel.SetActive(false);
        this.ModPanel.SetActive(true);
    }

    public void OnChooseFunction(Function f)
    {
        this.Ball.EasingFunction = f;
        this._FuncBtnText.text = f.ToString();
    }

    public void OnChooseModifier(Modifier m)
    {
        this.Ball.EasingModifier = m;
        this._ModBtnText.text = m.ToString();
    }

    public void FunctionChoosen(int i)
    {
        this.OnChooseFunction((Function)i);
        this.ToNormalView();
    }

    public void ModChoosen(int i)
    {
        this.OnChooseModifier((Modifier)i);
        this.ToNormalView();
    }

    public void ToNormalView()
    {
        this.BottomPanel.SetActive(true);
        this.FunctionPanel.SetActive(false);
        this.ModPanel.SetActive(false);
    }
}