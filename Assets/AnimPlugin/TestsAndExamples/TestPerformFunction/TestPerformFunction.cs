﻿using UnityEngine;
using System.Collections;
using AnimPlugin;


public class TestPerformFunction : MonoBehaviour
{

	void Start ()
	{
		this.StartCoroutine (
			AN.PerformFunction (
				F1,
				0,
				10
			)
		);


	}

	private int F1 (int i)
	{
		Debug.Log (i);
		i++;
		return i;
	}
}
