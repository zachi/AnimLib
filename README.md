# AnimLib for Unity
It's my pet-project originally started as a small lib to use [Easings] [1] interpolations in Unity's Lerp function.
Before anything else - add ```using AnimPlugin;``` to your code.
## Main features:
### Easy Lerp() replacement.
Replace:
```C#
Mathf.Lerp(min, max, time);
```
with:
```C#
A.Lerp(min, max, time);
```
then add any interpolation you like as fourth Lerp param.
```C#
A.Lerp(min, max, time, E.SineIn);
```
*HINT:* It also works with: **Vector2**, **Vector3**, **Vector4** and **Color Lerp** methods.
### Pausing any coroutine
Wrap your coroutines in **Pauseable** method, to be able to pause/continue them anytime.
Wrapping is as easy as changing from:
```C#
StartCoroutine(coroutine);
```
to
```C# 
StartCoroutine(A.Pauseable(IsPaused, coroutine));
```
and implement IsPaused method:
```C#
//returns true when left mouse button is pressed
bool IsPaused() {
    return Input.GetMouseButton(0);
}
```
### Includes basics commonly used moving/rotating/scaling and color changing over time methods.
```C#
// will move my transform to Vector.zero, and the move will take 5 secs.
StartCoroutine( A.MoveTowards(this.transform, Vector3.zero, 5f));
/// will rotate the other transform to desired angle with QuartInOut interpolation,
//animation will take 2 secs.
StartCoroutine( A.RotateTowards(other.transform, (Quaternion)desiredRotation, 2f, E.QuartInOut));
```

### Provides smart subsystem for chaining and parallel coroutines execution.
Basically - chain any number of **IEnumerator** coroutines functions, or orther them to be executed parallel, or wait with some method to be executed after all coroutines are completed, or play sound in the middle of coroutine, or just amaze me with your idea how to use it.
#### Chaining:
Any number of coroutines surrounded by ```A.Chain( )``` will be executed one after another.
#### Parallel:
Few coroutines can start at the same moment, as one coroutine, when surrounded by ```A.Parallel( )```. The _Parallel_ coroutine will finish, when all wrapped coroutine finished.
#### Helpers: Wait and Feedback
```A.Wait(float secs)``` and ```A.Feedback(Action action)``` are two little helper functions included to easy-chaining and receiving feedback from running coroutine. Use **Wait** for synchronization on chained/parallel coroutines, and **Feedback** to inline some code or call outside methods.

## Examples:
See the included **AnimPlugin/TestAndExamples/ExampleUsage.unity** for examples and usage.

[1]: http://easings.net/